
import java.util.Map;
public class Customer {
	private String Name;
	private String Gender;
	private int Age;
	private Map<String, Boolean> healthstatus;
	private Map<String, Boolean> habits;
	private double premium;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getGender() {
		return Gender;
	}
	public void setGender(String gender) {
		Gender = gender;
	}
	public int getAge() {
		return Age;
	}
	public void setAge(int age) {
		this.Age = age;
	}
	public Map<String, Boolean> getHealthstatus() {
		return healthstatus;
	}
	public void setHealthstatus(Map<String, Boolean> healthstatus) {
		this.healthstatus = healthstatus;
	}
	public Map<String, Boolean> getHabits() {
		return habits;
	}
	public void setHabits(Map<String, Boolean> habits) {
		this.habits = habits;
	}
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	public void setHealthStatus(Map<String, Boolean> healthCond) {
		// TODO Auto-generated method stub
		
	}
	
	

}
