import java.util.HashMap;
import java.util.Map;

public class InsuranceCalculator {

	public static void main(String[] args) {
		Customer cust = new Customer();
		cust.setName("Groome");
		cust.setAge(30);
		cust.setGender(Gender.MALE.toString());
		Map<String, Boolean> healthCond = new HashMap<>();
		healthCond.put(HealthCondition.HYPERTENSION.toString(), false);
		healthCond.put(HealthCondition.BLOOD_PRESSURE.toString(), false);
		healthCond.put(HealthCondition.BLOOD_SUGAR.toString(), false);
		healthCond.put(HealthCondition.OVERWEIGHT.toString(), false);
		
		cust.setHealthStatus(healthCond);
		
		Map<String, Boolean> habits = new HashMap<>();
		habits.put(Habits.BAD_HABIT_SMOKING.toString(), true);
		habits.put(Habits.BAD_HABIT_ALCOHOL.toString(), false);
		habits.put(Habits.GOOD_HABIT_DAILY_EXERCISE.toString(), true);
		habits.put(Habits.BAD_HABIT_DRUGS.toString(), false);
		
		cust.setHabits(habits);
		
		PremiumCalculator premiumCalc = new HabitsBasedcalculator(new HealthConditionCalculator(
				new AgeBasedPremiumCal(new GenderBasedPremiumCal(new BasicpremiumCal()))));
		premiumCalc.calculatePremium(cust);
		System.out.println("PREMIUM --- " + cust.getPremium());
	}

}