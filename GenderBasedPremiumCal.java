
public class GenderBasedPremiumCal extends PremiumCalculator {

		public GenderBasedPremiumCal(BasicpremiumCal basicpremiumCal) {
			super(basicpremiumCal);
		}
		
		@Override
		public Customer calculatePremium(Customer customer) {
			super.calculatePremium(customer);
			System.out.println("Gender Based premium calc..." + customer.getPremium());
			switch (customer.getGender()) {
			case "MALE":
				customer.setPremium(customer.getPremium() + (customer.getPremium() * 2/100));
				break;
			case "FEMALE":
			case "OTHER":
			default:
				break;
			}
			return customer;
		}

	}

	