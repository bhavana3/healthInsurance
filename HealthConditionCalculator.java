
import java.util.Iterator;
import java.util.Set;

public class HealthConditionCalculator extends PremiumCalculator {

	public HealthConditionCalculator(AgeBasedPremiumCal ageBasedPremiumCal) {
		super(ageBasedPremiumCal);
	}
	
	

	@Override
	public Customer calculatePremium(Customer customer) {
		super.calculatePremium(customer);
		System.out.println("Health Condition Based premium calc...");
		Set<String> healthCond = customer.getHealthstatus().keySet();
		Iterator<String> healthItr = healthCond.iterator();
		String healthCondName = "";
		while(healthItr.hasNext()){
			healthCondName = healthItr.next();
			if(customer.getHealthstatus().get(healthCondName)){
				customer.setPremium(customer.getPremium() + (customer.getPremium() * 1/100));
			}
		}
		return customer;
	}
}